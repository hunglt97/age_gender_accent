import argparse
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.optimizers.schedules import ExponentialDecay
from tensorflow.keras.optimizers import SGD, Adam
import numpy as np
from models import smol_xvec, medium_sized_nn, model_3_multitask
from utils import chop
from load_audio import load_audio
from get_label import LABELS, ROOT
import random


def tflite_convert(model, model_name):
    # Convert the model.
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    tflite_model = converter.convert()

    # Save the model.
    with open('./model/'+model_name+'.tflite', 'wb') as f:
        f.write(tflite_model)


def tflite_infer(input_data, model_name):
    tf_model = tf.lite.Interpreter('./model/'+model_name+'.tflite')
    input_details = tf_model.get_input_details()
    output_details = tf_model.get_output_details()
    print("====================================================")
    # print(input_details)
    # print("====================================================")
    # print(output_details)
    # print("====================================================")
    tf_model.allocate_tensors()
    tf_model.set_tensor(input_details[0]['index'], input_data)
    tf_model.invoke()
    age = tf_model.get_tensor(output_details[1]['index'])
    if np.argmax(age) == 0:
        predicted_age = "child"
    elif np.argmax(age) == 1:
        predicted_age = "adult"
    elif np.argmax(age) == 2:
        predicted_age = "old"
    else:
        raise ValueError
    gender = tf_model.get_tensor(output_details[2]['index'])
    if np.argmax(gender) == 0:
        predicted_gender = "female"
    elif np.argmax(gender) == 1:
        predicted_gender = "male"
    else:
        raise ValueError
    accent = tf_model.get_tensor(output_details[0]['index'])
    if np.argmax(accent) == 0:
        predicted_accent = "North"
    elif np.argmax(accent) == 1:
        predicted_accent = "Central"
    elif np.argmax(accent) == 2:
        predicted_accent = "South"
    else:
        raise ValueError
    print("Age: {} \t\t Predicted: {}".format(age, predicted_age))
    print("Gender: {} \t\t Predicted: {}".format(gender, predicted_gender))
    print("Accent: {} \t\t Predicted: {}".format(accent, predicted_accent))


def _get_parser():
    parser = argparse.ArgumentParser(description="Arguments")
    parser.add_argument('--model_name', type=str, default="weights_multitask_250_13_2021-11-15")
    parser.add_argument('--write-model', dest='write_model', action='store_true')
    parser.set_defaults(write_model=False)
    parser.add_argument('--test', dest='test', action='store_true')
    parser.set_defaults(test=False)
    return parser


def main():
    parser = _get_parser()
    args = parser.parse_args()
    if args.write_model:
        model = model_3_multitask(1, 200, 13)
        model.load_weights("./model/" + args.model_name + ".h5")
        tflite_convert(model, args.model_name)

    if args.test:
        test_data_path = "./all_data.txt"
        with open(test_data_path) as f:
            all_files = f.readlines()
            file = random.choice(all_files)
            path = file.split()[0]
            label = file.split()[1]
            print("Test file: {}".format(path))
            print("True label: {}".format(label))
            x = load_audio(ROOT, path, n_mfcc=13)
            x = chop(x, chop_length=200)
            tflite_infer(x, args.model_name)


if __name__ == "__main__":
    main()
