import numpy as np
from collections import Counter
import tensorflow.keras as keras
from sklearn.model_selection import train_test_split
from get_label import LABELS, ROOT
from extract_feature import extract_feature, extract_feature_sliding


def read_data_label(classifier, full=True, time_step=500, n_mfcc=13, val_ratio=0.1):
    print("====================================================")
    print("Start Reading Labels...")
    all_data_path = ("./all_data.txt" if full else "./all_data_small.txt")
    num_classes = 18
    all_paths = []
    all_labels = []
    with open(all_data_path) as f:
        all_files = f.readlines()
        for file in all_files:
            all_paths.append(file.split()[0])
            label = LABELS.index(file.split()[1])
            if classifier == 'gender':
                num_classes = 2
                if label in [0, 1, 2, 6, 7, 8, 12, 13, 14]:
                    label = 1
                elif label in [3, 4, 5, 9, 10, 11, 15, 16, 17]:
                    label = 0
            elif classifier == 'age':
                num_classes = 3
                if label in [0, 1, 2, 3, 4, 5]:
                    label = 0
                elif label in [6, 7, 8, 9, 10, 11]:
                    label = 1
                elif label in [12, 13, 14, 15, 16, 17]:
                    label = 2
            elif classifier == 'accent':
                num_classes = 3
                if label in [0, 3, 6, 9, 12, 15]:
                    label = 0
                elif label in [1, 4, 7, 10, 13, 16]:
                    label = 1
                elif label in [2, 5, 8, 11, 14, 17]:
                    label = 2
            all_labels.append(label)
    print("Total Data: {}".format(len(all_paths)))
    c = Counter(all_labels)
    for label in range(num_classes):
        print("Label {}: {}".format(label, c[label]))
    print("Reading Labels Completed")

    print("====================================================")
    print("Start Extracting Features...")
    all_feature = extract_feature(ROOT, all_paths, full=full, time_step=time_step, n_mfcc=n_mfcc)
    print("Extract Features Completed")
    print("====================================================")
    print("Start Splitting Train Test Sets...")
    x_train, x_val, y_train, y_val = train_test_split(all_feature, all_labels, test_size=val_ratio)
    print("Splitting Train Test Sets Completed")
    return x_train, x_val, y_train, y_val


def read_data_label_multitask(full=True, time_step=500, n_mfcc=13, val_ratio=0.1):
    print("====================================================")
    print("Start Reading Labels...")
    all_data_path = ("./all_data.txt" if full else "./all_data_small.txt")
    all_paths = []
    age_labels = []
    gender_labels = []
    accent_labels = []
    all_labels = []
    with open(all_data_path) as f:
        all_files = f.readlines()
        for file in all_files:
            all_paths.append(file.split()[0])
            label = LABELS.index(file.split()[1])
            # Gender label
            if label in [0, 1, 2, 6, 7, 8, 12, 13, 14]:
                gender_labels.append(1)
            elif label in [3, 4, 5, 9, 10, 11, 15, 16, 17]:
                gender_labels.append(0)
            # Age label
            if label in [0, 1, 2, 3, 4, 5]:
                age_labels.append(0)
            elif label in [6, 7, 8, 9, 10, 11]:
                age_labels.append(1)
            elif label in [12, 13, 14, 15, 16, 17]:
                age_labels.append(2)
            # Accent label
            if label in [0, 3, 6, 9, 12, 15]:
                accent_labels.append(0)
            elif label in [1, 4, 7, 10, 13, 16]:
                accent_labels.append(1)
            elif label in [2, 5, 8, 11, 14, 17]:
                accent_labels.append(2)
    print("Total Data: {}".format(len(all_paths)))
    for i in range(len(age_labels)):
        all_labels.append((age_labels[i], gender_labels[i], accent_labels[i]))
    age_count = Counter(age_labels)
    gender_count = Counter(gender_labels)
    accent_count = Counter(accent_labels)
    for label in range(3):
        print("Age Label {}: {}".format(label, age_count[label]))

    for label in range(2):
        print("Gender Label {}: {}".format(label, gender_count[label]))

    for label in range(3):
        print("Accent Label {}: {}".format(label, accent_count[label]))
    print("Reading Labels Completed")

    print("====================================================")
    print("Start Extracting Features...")
    # all_feature = extract_feature(ROOT, all_paths, full=full, time_step=time_step, n_mfcc=n_mfcc)
    all_feature, all_labels = extract_feature_sliding(ROOT, all_paths, all_labels, full=full, time_step=time_step, n_mfcc=n_mfcc)

    print("feature: {}".format(all_feature.shape))
    print("label: {}".format(all_labels.shape))

    print("Extract Features Completed")
    print("====================================================")
    print("Start Splitting Train Test Sets...")
    x_train, x_val, y_train, y_val = train_test_split(all_feature, np.array(all_labels), test_size=val_ratio)
    print("Splitting Train Test Sets Completed")
    return x_train, x_val, y_train, y_val


class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'

    def __init__(self, features, labels, batch_size=64, time_step=500, dim=13,
                 n_classes=2, shuffle=True):
        'Initialization'
        self.time_step = time_step
        self.dim = dim
        self.input_shape = (time_step, dim)
        self.batch_size = batch_size
        self.labels = labels
        self.features = features
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.indexes = np.arange(len(self.features))
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.features) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_x = self.features[index * self.batch_size:(index + 1) * self.batch_size]
        if type(self.labels) is list:
            batch_y = self.labels[index * self.batch_size:(index + 1) * self.batch_size]
            return batch_x, keras.utils.to_categorical(batch_y, num_classes=self.n_classes)
        elif type(self.labels) is not list:
            # print(self.labels.shape)
            batch_y0 = self.labels[index * self.batch_size:(index + 1) * self.batch_size, 0].tolist()
            batch_y1 = self.labels[index * self.batch_size:(index + 1) * self.batch_size, 1].tolist()
            batch_y2 = self.labels[index * self.batch_size:(index + 1) * self.batch_size, 2].tolist()
            # print(batch_y0.shape)
            batch_y0 = keras.utils.to_categorical(batch_y0, num_classes=3)
            batch_y1 = keras.utils.to_categorical(batch_y1, num_classes=2)
            batch_y2 = keras.utils.to_categorical(batch_y2, num_classes=3)
            return batch_x, {'age_output': batch_y0, 'gender_output': batch_y1, 'accent_output': batch_y2}
        else:
            raise ValueError('Undefined labels')

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.features))
        if self.shuffle:
            np.random.shuffle(self.indexes)
