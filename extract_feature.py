import os
import numpy as np
from load_audio import load_audio
from utils import chop, sliding


def extract_feature(root, all_paths, full=True, time_step=500, n_mfcc=13):
    if full:
        feature_path = "./all_features_" + str(time_step) + "ms_" + str(n_mfcc) + "mfcc" + ".npy"
    else:
        feature_path = "./all_features_small_" + str(time_step) + "ms_" + str(n_mfcc) + "mfcc" + ".npy"
    if os.path.isfile(feature_path):
        print("Feature file existed! Loading...")
        return np.load(feature_path)
    else:
        output = np.zeros((len(all_paths), time_step, n_mfcc))
        for i, path in enumerate(all_paths):
            x = load_audio(root, path, n_mfcc=13)
            if x.shape[1] < 100:
                continue
            x = chop(x, chop_length=time_step)
            output[i, :, :] = x
            print("Progress: {}/{}".format(i, len(all_paths)), end="\r")
        np.save(feature_path, output)
        return output


def extract_feature_sliding(root, all_paths, all_labels, full=True, time_step=500, n_mfcc=13):
    if full:
        feature_path = "./all_features_" + str(time_step) + "ms_" + str(n_mfcc) + "mfcc" + ".npy"
        label_path = "./all_labels_" + str(time_step) + "ms_" + str(n_mfcc) + "mfcc" + ".npy"
    else:
        feature_path = "./all_features_small_" + str(time_step) + "ms_" + str(n_mfcc) + "mfcc" + ".npy"
        label_path = "./all_labels_small_" + str(time_step) + "ms_" + str(n_mfcc) + "mfcc" + ".npy"
    if os.path.isfile(feature_path) and os.path.isfile(label_path):
        print("Feature file existed! Loading...")
        return np.load(feature_path), np.load(label_path)
    else:
        # output_features = np.zeros((len(all_paths), time_step, n_mfcc))
        # output_labels = np.zeros((len(all_paths)))
        output_features = []
        output_labels = []
        for i, (path, label) in enumerate(zip(all_paths, all_labels)):
            x = load_audio(root, path, n_mfcc=13)
            if x.shape[1] < 100:
                continue
            features = sliding(x, time_step, stride=time_step, keep_last=False)
            for f in features:
                output_features.append(f)
                output_labels.append(label)
            print("Progress: {}/{}".format(i, len(all_paths)), end="\r")
        print("Total extracted files: {}".format(len(output_features)))
        print("Total extracted labels: {}".format(len(output_labels)))
        output_features = np.array(output_features).squeeze()
        output_labels = np.array(output_labels)
        np.save(feature_path, output_features)
        np.save(label_path, output_labels)
        return output_features, output_labels

