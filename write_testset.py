import pandas as pd
import random

ROOT = "/data1/T30/speechData/data_Vi/"
data_path = "./csv/"
LABELS = ["cmn", "cmc", "cms", "cfn", "cfc", "cfs",
          "amn", "amc", "ams", "afn", "afc", "afs",
          "omn", "omc", "oms", "ofn", "ofc", "ofs"]


def write_data_label_age_test(n_utt=10000):
    child = []
    adult = []
    old = []
    for label in LABELS:
        if label in ["cmn", "cmc", "cms", "cfn", "cfc", "cfs"]:
            df = pd.read_csv(data_path+label+".csv", dtype=str)
            paths = df.path_wav.to_list()
            child = child + paths
        elif label in ["amn", "amc", "ams", "afn", "afc", "afs"]:
            df = pd.read_csv(data_path + label + ".csv", dtype=str)
            paths = df.path_wav.to_list()
            adult = adult + paths
        elif label in ["omn", "omc", "oms", "ofn", "ofc", "ofs"]:
            df = pd.read_csv(data_path + label + ".csv", dtype=str)
            paths = df.path_wav.to_list()
            old = old + paths
    child_l = []
    adult_l = []
    old_l = []
    child = random.choices(child, k=n_utt//3)
    for c in child:
        child_l.append([c, "c"])

    adult = random.choices(adult, k=n_utt // 3)
    for a in adult:
        adult_l.append([a, "a"])

    old = random.choices(old, k=n_utt // 3)
    for o in old:
        old_l.append([o, "o"])

    print("child: {}".format(len(child)))
    print("adult: {}".format(len(adult)))
    print("old: {}".format(len(old)))
    age = child_l + adult_l + old_l
    random.shuffle(age)
    print("total: {}".format(len(age)))
    df = pd.DataFrame(age, index=None, columns=["path_wav", "spk_age"])
    df.to_csv('age.csv', index=False)


def write_data_label_gender_test(n_utt=10000):
    male = []
    female = []
    for label in LABELS:
        if label in ["cmn", "cmc", "cms", "amn", "amc", "ams", "omn", "omc", "oms"]:
            df = pd.read_csv(data_path + label + ".csv", dtype=str)
            paths = df.path_wav.to_list()
            male = male + paths
        elif label in ["cfn", "cfc", "cfs", "afn", "afc", "afs", "ofn", "ofc", "ofs"]:
            df = pd.read_csv(data_path + label + ".csv", dtype=str)
            paths = df.path_wav.to_list()
            female = female + paths
    male_l = []
    female_l = []
    male = random.choices(male, k=n_utt // 2)
    for m in male:
        male_l.append([m, "m"])

    female = random.choices(female, k=n_utt // 2)
    for f in female:
        female_l.append([f, "f"])

    print("male: {}".format(len(male)))
    print("female: {}".format(len(female)))
    gender = female_l + male_l
    random.shuffle(gender)
    print("total: {}".format(len(gender)))
    df = pd.DataFrame(gender, index=None, columns=["path_wav", "spk_gender"])
    df.to_csv('gender.csv', index=False)


def write_data_label_accent_test(n_utt=10000):
    north = []
    central = []
    south = []
    for label in LABELS:
        if label in ["cmn", "cfn", "amn", "afn", "omn", "ofn"]:
            df = pd.read_csv(data_path + label + ".csv", dtype=str)
            paths = df.path_wav.to_list()
            north = north + paths
        elif label in ["cmc", "cfc", "amc", "afc", "omc", "ofc"]:
            df = pd.read_csv(data_path + label + ".csv", dtype=str)
            paths = df.path_wav.to_list()
            central = central + paths
        elif label in ["cms", "cfs", "ams", "afs", "oms", "ofs"]:
            df = pd.read_csv(data_path + label + ".csv", dtype=str)
            paths = df.path_wav.to_list()
            south = south + paths
    north_l = []
    central_l = []
    south_l = []
    north = random.choices(north, k=n_utt // 3)
    for n in north:
        north_l.append([n, "n"])

    central = random.choices(central, k=n_utt // 3)
    for c in central:
        central_l.append([c, "c"])

    south = random.choices(south, k=n_utt // 3)
    for s in south:
        south_l.append([s, "s"])

    print("north: {}".format(len(north)))
    print("central: {}".format(len(central)))
    print("south: {}".format(len(south)))
    accent = north_l + central_l + south_l
    random.shuffle(accent)
    print("total: {}".format(len(accent)))
    df = pd.DataFrame(accent, index=None, columns=["path_wav", "spk_accent"])
    df.to_csv('accent.csv', index=False)


def main():
    # if not os.path.isfile("/data/speechDataTeam/hunglt16/all.txt"):
    write_data_label_age_test(n_utt=10000)
    write_data_label_gender_test(n_utt=10000)
    write_data_label_accent_test(n_utt=10000)


if __name__ == "__main__":
    main()