import os
import argparse
import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
import logging
from models import smol_xvec, medium_sized_nn, model_3_multitask
from get_label import write_data_label
from data_loader import read_data_label, read_data_label_multitask, DataGenerator
import datetime

# os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# tf.debugging.set_log_device_placement(True)

logging.basicConfig(level=logging.INFO)
date = datetime.datetime.now()
date = str(date).split(" ")[0]


def plot_loss(history):
    logging.info('Start ploting loss...')
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs = range(len(acc))

    plt.subplot(1, 2, 1)
    plt.plot(epochs, acc, 'r', label='Training accuracy')
    plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
    plt.title('Training and validation accuracy')
    plt.legend(loc=0)
    # plt.figure()

    plt.subplot(1, 2, 2)
    plt.plot(epochs, loss, 'r', label='Training loss')
    plt.plot(epochs, val_loss, 'b', label='Validation loss')
    plt.title('Training and validation loss')
    plt.legend(loc=0)
    # plt.figure()

    plt.show()


def _get_parser():
    parser = argparse.ArgumentParser(description="Trainer Arguments")
    parser.add_argument('--full_data', dest='full_data', action='store_true')
    parser.add_argument('--small_data', dest='full_data', action='store_false')
    parser.set_defaults(full_data=True)
    parser.add_argument('--limit_growth', dest='limit_growth', action='store_true')
    parser.add_argument('--unlimited_growth', dest='limit_growth', action='store_false')
    parser.set_defaults(limit_growth=False)
    parser.add_argument('--from_checkpoint', type=str, default=None)
    parser.add_argument('--classifier', choices=['age', 'gender', 'accent'], default='gender')
    parser.add_argument('--model_name', type=str, default='smol_xvec')
    parser.add_argument('--time_step', type=int, default=500)
    parser.add_argument('--dim', type=tuple, default=13)
    parser.add_argument('--batch_size', type=int, default=128)
    parser.add_argument('--epoch', type=int, default=30)

    return parser


def main():
    parser = _get_parser()
    args = parser.parse_args()
    gpus = tf.config.list_physical_devices('GPU')
    if gpus and args.limit_growth:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)
        except:
            pass

    # Get label
    if not os.path.isfile("./all_data.txt") and args.full_data:
        write_data_label()
    elif not os.path.isfile("./all_data_small.txt") and not args.full_data:
        write_data_label(full=False)

    # Get data
    if args.model_name == 'multitask':
        x_train, x_val, y_train, y_val = read_data_label_multitask(args.full_data,
                                                                   args.time_step,
                                                                   args.dim,
                                                                   val_ratio=0.05)
    else:
        x_train, x_val, y_train, y_val = read_data_label(args.classifier,
                                                         args.full_data,
                                                         args.time_step,
                                                         args.dim,
                                                         val_ratio=0.05)
    if args.classifier == 'all':
        num_classes = 18
    elif args.classifier == 'gender':
        num_classes = 2
    elif args.classifier == 'age' or args.classifier == 'accent':
        num_classes = 3
    else:
        raise ValueError('Unknown classifier')

    # Create model
    if args.model_name == 'smol_xvec':
        model = smol_xvec(args.batch_size, args.time_step, args.dim, num_classes)
    elif args.model_name == 'multitask':
        model = model_3_multitask(args.batch_size, args.time_step, args.dim)
    elif args.model_size == 'medium':
        model = medium_sized_nn(args.batch_size, args.time_step, args.dim, num_classes)
    else:
        raise NameError('Model not implemented')
    if args.from_checkpoint is not None:
        model.load_weights(args.from_checkpoint)
    model.summary()

    if args.model_name == 'multitask':
        checkpoint_path = "./model/weights_"+model.name+"_"+str(args.time_step)+"_"+str(args.dim)+"_"+date+".h5"
    else:
        checkpoint_path = "./model/weights_"+model.name+"_"+args.classifier+"_"+str(args.time_step)+"_"+str(args.dim)+"_"+date+".h5"
    duplicates = 0
    while os.path.isfile(checkpoint_path):
        duplicates += 1
        # old_model_path = checkpoint_path
        if args.model_name == 'multitask':
            checkpoint_path = "./model/weights_" + model.name + "_" + str(args.time_step) + "_" + str(
                args.dim) + "_" + date + "_" + str(duplicates) + ".h5"
        else:
            checkpoint_path = "./model/weights_" + model.name + "_" + args.classifier + "_" + str(
                args.time_step) + "_" + str(args.dim) + "_" + date + "_" + str(duplicates) + ".h5"

    callback = ModelCheckpoint(filepath=checkpoint_path,
                               monitor="val_loss",
                               save_best_only=True,
                               save_weights_only=True,
                               verbose=1)

    # ====================
    # train
    # ====================

    training_generator = DataGenerator(x_train,
                                       y_train,
                                       batch_size=args.batch_size,
                                       time_step=args.time_step,
                                       dim=args.dim,
                                       n_classes=num_classes)
    validation_generator = DataGenerator(x_val,
                                         y_val,
                                         batch_size=args.batch_size,
                                         time_step=args.time_step,
                                         dim=args.dim,
                                         n_classes=num_classes)

    logging.info('Start training...')
    history = model.fit(training_generator,
                        epochs=args.epoch,
                        validation_data=validation_generator,
                        callbacks=[callback],
                        # workers=4,
                        # use_multiprocessing=True,
                        max_queue_size=128)
    logging.info('Training finished!')
    # plot_loss(history)
    if args.model_name == 'multitask':
        model.save("./model/model_"+model.name+"_"+str(args.time_step)+"_"+str(args.dim)+"_"+date+".h5")
    else:
        model.save("./model/model_"+model.name+"_"+args.classifier+"_"+str(args.time_step)+"_"+str(args.dim)+"_"+date+".h5")


if __name__ == "__main__":
    main()
