import os
import argparse
import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
import logging
import pandas as pd
from utils import sliding
from load_audio import load_audio
from models import smol_xvec, medium_sized_nn, model_3_multitask
from get_label import write_data_label
from data_loader import read_data_label, read_data_label_multitask, DataGenerator
import datetime

# os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# tf.debugging.set_log_device_placement(True)

logging.basicConfig(level=logging.INFO)
date = datetime.datetime.now()
date = str(date).split(" ")[0]


def test(test_model, test_for, threshold=0.6, min_length=1, limit=None, print_details=True):
    audio_path = "./csv_fix/test/"
    alarm = 0
    count = 0
    total_duration = 0
    df = pd.DataFrame()
    for root, dirs, files in os.walk(audio_path):
        for file in files:
            name, extension = os.path.splitext(file)
            if extension == '.wav' and "tmp_file" not in name:
                print(name)
                feature = load_audio(root, file)

                result = []

                chunks = sliding(feature, window_size=120, stride=10)
                for time, chunk in enumerate(chunks):
                    if chunk.shape[1] == 120:
                        chunk = chunk[np.newaxis, :, :, np.newaxis]
                        # chunk = np.concatenate([chunk, mask], axis=3)
                        out = test_model.predict(chunk)
                        result.append(out)
                df.add((result))
                count += 1
            if count == limit:
                break
    print("alarm: {}".format(alarm))
    print("total: {}".format(count))
    print("total duration: " + str(datetime.timedelta(seconds=total_duration)))
    return alarm


def _get_parser():
    parser = argparse.ArgumentParser(description="Trainer Arguments")
    parser.add_argument('--limit_growth', type=bool, default=False)
    parser.add_argument('--from_checkpoint', type=str, default=None)
    parser.add_argument('--classifier', choices=['age', 'gender', 'accent'], default='gender')
    parser.add_argument('--model_name', type=str, default='smol_xvec')
    parser.add_argument('--time_step', type=int, default=500)
    parser.add_argument('--dim', type=tuple, default=13)
    parser.add_argument('--batch_size', type=int, default=128)
    parser.add_argument('--epoch', type=int, default=30)
    parser.add_argument('--save_best', type=bool, default=True)

    return parser

def main():
    parser = _get_parser()
    args = parser.parse_args()
    gpus = tf.config.list_physical_devices('GPU')
    if gpus and args.limit_growth:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)
        except:
            pass

    # Get label
    if not os.path.isfile("./all_data.txt"):
        write_data_label()

    # Get data
    if args.model_name == 'multitask':
        x_train, x_val, y_train, y_val = read_data_label_multitask(args.time_step, args.dim, val_ratio=0.05)
    else:
        x_train, x_val, y_train, y_val = read_data_label(args.classifier, args.time_step, args.dim, val_ratio=0.05)
    if args.classifier == 'all':
        num_classes = 18
    elif args.classifier == 'gender':
        num_classes = 2
    elif args.classifier == 'age' or args.classifier == 'accent':
        num_classes = 3
    else:
        raise ValueError('Unknown classifier')

    # Create model
    if args.model_name == 'smol_xvec':
        model = smol_xvec(args.batch_size, args.time_step, args.dim, num_classes)
    elif args.model_name == 'multitask':
        model = model_3_multitask(args.batch_size, args.time_step, args.dim)
    elif args.model_size == 'medium':
        model = medium_sized_nn(args.batch_size, args.time_step, args.dim, num_classes)
    else:
        raise NameError('Model not implemented')
    model.summary()
    checkpoint_path = "./model/very_smol_nn_weights_" + model.name + "_" + date + ".h5"
    checkpoint_dir = os.path.dirname(checkpoint_path)

    callback = ModelCheckpoint(filepath=checkpoint_path,
                               monitor="val_loss",
                               save_best_only=True,
                               save_weights_only=True,
                               verbose=1)

    # ====================
    # train
    # ====================

    training_generator = DataGenerator(x_train,
                                       y_train,
                                       batch_size=args.batch_size,
                                       time_step=args.time_step,
                                       dim=args.dim,
                                       n_classes=num_classes)
    validation_generator = DataGenerator(x_val,
                                         y_val,
                                         batch_size=args.batch_size,
                                         time_step=args.time_step,
                                         dim=args.dim,
                                         n_classes=num_classes)

    logging.info('Start training...')
    history = model.fit(training_generator,
                        epochs=args.epoch,
                        validation_data=validation_generator,
                        callbacks=[callback],
                        # workers=4,
                        # use_multiprocessing=True,
                        max_queue_size=128)
    logging.info('Training finished!')
    model.save("./model/very_smol_nn_model_" + model.name + "_" + date + ".h5")


if __name__ == "__main__":
    main()
