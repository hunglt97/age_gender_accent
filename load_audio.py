import numpy as np
import librosa
import os
from utils import norm


def load_audio(root, file, n_mfcc=13):
    signal, _ = librosa.load(
        os.path.join(root, file),
        sr=16000,
        mono=True,
        offset=0.0,
        res_type='kaiser_best',
    )
    feature = librosa.feature.mfcc(
        signal,
        sr=16000,
        S=None,
        hop_length=160,
        n_mfcc=n_mfcc,
        dct_type=2,
        norm="ortho",
        lifter=0,
        n_mels=128,
        fmin=0.0,
        fmax=None,
        htk=False,
    )
    feature = norm(feature)
    feature = np.transpose(feature)
    feature = feature[np.newaxis, :, :]
    return feature
