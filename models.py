import tensorflow as tf
from tensorflow.keras import layers, models
from tensorflow.keras.optimizers.schedules import ExponentialDecay
from tensorflow.keras.optimizers import SGD, Adam


class StatsPooling(layers.Layer):
    def __init__(self):
        super(StatsPooling, self).__init__()

    def call(self, inputs, **kwargs):
        mean, std = tf.nn.moments(inputs, axes=-2)
        outputs = tf.concat([mean, std + 1e-6], -1)
        return outputs


def smol_xvec(batch_size, time_step, dim, num_classes=18):
    input_shape = (time_step, dim)
    inputs = layers.Input(shape=input_shape, batch_size=batch_size)
    x = layers.Conv1D(128, 15, strides=3, activation='relu', input_shape=input_shape)(inputs)
    x = layers.Conv1D(128, 5, strides=2, activation='relu', input_shape=input_shape)(x)
    x = layers.Conv1D(128, 3, strides=2, activation='relu', input_shape=input_shape)(x)
    x = layers.Dense(128, activation='relu')(x)
    x = layers.BatchNormalization()(x)
    x = StatsPooling()(x)
    x = layers.Dense(128, activation='relu')(x)
    outputs = layers.Dense(num_classes, activation='softmax')(x)
    model = models.Model(inputs, outputs, name='smol_xvec')
    lr_schedule = ExponentialDecay(
        initial_learning_rate=1e-4,
        decay_steps=500000,
        decay_rate=0.1)
    opt = Adam(learning_rate=lr_schedule)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    return model


def medium_sized_nn(batch_size, time_step, dim, num_classes=18):
    input_shape = (time_step, dim)
    inputs = layers.Input(shape=input_shape, batch_size=batch_size)
    x = layers.Conv1D(512, 15, strides=3, activation='relu', input_shape=input_shape)(inputs)
    x = layers.Conv1D(512, 5, strides=2, activation='relu', input_shape=input_shape)(x)
    x = layers.Conv1D(512, 3, strides=2, activation='relu', input_shape=input_shape)(x)
    x = layers.Conv1D(512, 1, strides=1, activation='relu', input_shape=input_shape)(x)
    x = layers.Dense(512, activation='relu')(x)
    x = StatsPooling()(x)
    x = layers.Dense(512, activation='relu')(x)
    x = layers.Dense(512, activation='relu')(x)
    outputs = layers.Dense(num_classes, activation='softmax')(x)
    model = models.Model(inputs, outputs, name='medium_sized_nn')
    lr_schedule = ExponentialDecay(
        initial_learning_rate=1e-4,
        decay_steps=10000,
        decay_rate=0.99)
    opt = Adam(learning_rate=lr_schedule)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    return model


def residual_block(x, filters, conv_num=3, activation="relu"):
    # Shortcut
    s = layers.Conv1D(filters, 1, padding="same")(x)
    for i in range(conv_num - 1):
        x = layers.Conv1D(filters, 3, padding="same")(x)
        x = layers.Activation(activation)(x)
    x = layers.Conv1D(filters, 3, padding="same")(x)
    x = layers.Add()([x, s])
    x = layers.Activation(activation)(x)
    return layers.MaxPool1D(pool_size=2, strides=2)(x)


def model_4(batch_size, time_step, dim, n_classes=2):
    input_shape = (time_step, dim)
    inputs = layers.Input(shape=input_shape, batch_size=batch_size)

    x = residual_block(inputs, 16, 2)
    x = residual_block(x, 32, 2)
    x = residual_block(x, 64, 3)
    x = residual_block(x, 128, 3)
    x = residual_block(x, 128, 3)

    x = layers.AveragePooling1D(pool_size=3, strides=3)(x)
    x = layers.Flatten()(x)
    x = layers.Dense(256, activation="relu")(x)
    x = layers.Dense(128, activation="relu")(x)

    outputs = layers.Dense(n_classes, activation="softmax", name="output")(x)
    model = models.Model(inputs, outputs, name='another_model')
    lr_schedule = ExponentialDecay(
        initial_learning_rate=1e-4,
        decay_steps=10000,
        decay_rate=0.99)
    opt = Adam(learning_rate=lr_schedule)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    return model


def model_2(batch_size, time_step, dim, n_classes=2):
    input_shape = (time_step, dim)
    inputs = layers.Input(shape=input_shape, batch_size=batch_size)
    #     x = layers.Flatten()(inputs)
    x = layers.Conv2D(32, kernel_size=(3, 3), activation='relu',
                      input_shape=input_shape[1:])(inputs)
    x = layers.MaxPooling2D(pool_size=(2, 2))(x)
    x = layers.Conv2D(64, kernel_size=(3, 3), activation='relu')(x)
    x = layers.MaxPooling2D(pool_size=(2, 2))(x)
    x = layers.Dropout(0.25)(x)
    x = layers.Flatten()(x)
    x = layers.Dense(128, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = layers.Dense(n_classes, activation='softmax')
    #     model.compile(loss='categorical_crossentropy',
    #                   optimizer='adadelta',
    #                   metrics=['accuracy'])
    model = models.Model(inputs, outputs, name='cnn_2D')
    lr_schedule = ExponentialDecay(
        initial_learning_rate=1e-4,
        decay_steps=10000,
        decay_rate=0.99)
    opt = Adam(learning_rate=lr_schedule)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

    return model


def model_3_multitask(batch_size, time_step, dim):
    input_shape = (time_step, dim)
    inputs = layers.Input(shape=input_shape, batch_size=batch_size)
    #     x = layers.Flatten()(inputs)
    x = layers.Conv1D(128, 15, strides=3, activation='relu', input_shape=input_shape)(inputs)
    # x = layers.Dropout(0.2)(x)
    x = layers.Conv1D(128, 5, strides=2, activation='relu', input_shape=input_shape)(x)
    # x = layers.Dropout(0.2)(x)
    x = layers.Conv1D(128, 3, strides=2, activation='relu', input_shape=input_shape)(x)
    # x = layers.Dropout(0.2)(x)
    x = layers.BatchNormalization()(x)
    x = layers.Dense(128, activation='relu')(x)
    # x = layers.Dropout(0.2)(x)
    x = StatsPooling()(x)

    # Age branch
    x1 = layers.Dense(128, activation='relu')(x)
    age = layers.Dense(3, activation='softmax', name='age_output')(x1)

    # Gender branch
    x2 = layers.Dense(128, activation='relu')(x)
    gender = layers.Dense(2, activation='softmax', name='gender_output')(x2)

    # Accent branch
    x3 = layers.Dense(128, activation='relu')(x)
    accent = layers.Dense(3, activation='softmax', name='accent_output')(x3)

    model = models.Model(inputs, outputs=[age, gender, accent], name='multitask')
    lr_schedule = ExponentialDecay(
        initial_learning_rate=1e-4,
        decay_steps=1000000,
        decay_rate=0.1)
    opt = Adam(learning_rate=lr_schedule)
    model.compile(loss={'age_output': 'categorical_crossentropy',
                        'gender_output': 'categorical_crossentropy',
                        'accent_output': 'categorical_crossentropy'},
                  loss_weights={'age_output': 0.3,
                                'gender_output': 0.3,
                                'accent_output': 0.4},
                  optimizer=opt, metrics=['accuracy'])

    return model
