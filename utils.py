import numpy as np
import random


def sliding(feature, window_size=120, stride=10, keep_last=True):
    length = feature.shape[1]
    count = 0
    result = []
    while length > 0:
        start = stride*count
        end = np.minimum(stride*count+window_size, feature.shape[1])
#         if not keep_last and (end - start < window_size) and (start - stride + window_size != feature.shape[1]):
#             break
        if not keep_last and (end - start < window_size):
            break
        feature_temp = feature[:, start:end, np.newaxis]
        length -= stride
        count += 1
        result.append(feature_temp)
        if end - start < window_size:
            break
    return result


def chop(feature, chop_length=500):
    length = feature.shape[1]
    if length < chop_length:
        diff = chop_length-length
        # padding = np.zeros((feature.shape[0], diff, feature.shape[2]))
        padding = np.random.random((feature.shape[0], diff, feature.shape[2])) / 1e6
        return np.concatenate((feature, padding), axis=1)
    elif length >= chop_length:
        diff = length-chop_length
        return feature[:,(diff//2):(diff//2+chop_length),:]


def norm(x):
    x[x == 0] = 0.000001
    x[x == -np.Inf] = -30
    x[x != x] = -30
    x[x == np.Inf] = 0
    return x
