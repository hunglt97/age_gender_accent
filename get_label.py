import os
import pandas as pd
import random

ROOT = "/data1/T30/speechData/data_Vi/"
data_path = "./csv_fix/"
LABELS = ["cmn", "cmc", "cms", "cfn", "cfc", "cfs",
          "amn", "amc", "ams", "afn", "afc", "afs",
          "omn", "omc", "oms", "ofn", "ofc", "ofs"]


def write_data_label(full=True):
    all_data_path = ("./all_data.txt" if full else "./all_data_small.txt")
    with open(all_data_path, "w+") as f:
        for label in LABELS:
            df = pd.read_csv(data_path+label+".csv", dtype=str)
            paths = df.path_wav.to_list()
            if full:
                f.writelines("%s\t%s\n" % (path, label) for path in paths)
            else:
                f.writelines("%s\t%s\n" % (path, label) for path in paths[:5000])


def main():
    write_data_label()


if __name__ == "__main__":
    main()
